#!/usr/bin/env python
# -*- coding: 850 -*-
# -*- coding: utf-8 -*-
# encoding: latin1


import math
import cv2
import subprocess
import os

#diccionario de todo los contornos
contours = {}
#conjunto de bordes de poligono
approx = []
#escala de los texto
scale = 2
#camara
cap = cv2.VideoCapture(0)
print("presiona q para salir")

#definir el codec y crear el objeto videowriter
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output.avi', fourcc, 20.0, (800,640))

#calculamos el �ngulo

def angle(pt1,pt2,pt0):
    dx1 = pt1[0][0] - pt0[0][0]
    dy1 = pt1[0][1] - pt0[0][1]
    dx2 = pt2[0][0] - pt0[0][0]
    dy2 = pt2[0][1] - pt0[0][1]
    return float((dx1*dx2 + dy1*dy2))/math.sqrt(float((dx1*dx1 + dy1*dy1))*(dx2*dx2 + dy2*dy2) + 1e-10)


while(cap.isOpened()):
    #capturamos frame por frame
    ret, frame = cap.read()
    if ret == True:
        #escala de grices
        gray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
        #astuto
        canny = cv2.Canny(frame, 80, 240, 3)

        #contornos
        canny2, contours, hierarchy = cv2.findContours(canny, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        for i in range(0, len(contours)):
            #aproximar el contorno con una precisi�n proporcional al perimetro del control
            approx = cv2.approxPolyDP(contours[i], cv2.arcLength(contours[i], True)*0.02,True)

            #omitir objetos peque�os o no convexos
            if(abs(cv2.contourArea(contours[i])) < 100 or not(cv2.isContourConvex(approx))):
                continue

            #tri�ngulo
            if(len(approx) == 3):
                x, y, w, h = cv2.boundingRect(contours[i])
                frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
                cv2.putText(frame, 'Tri', (x,y), cv2.FONT_HERSHEY_SIMPLEX, scale, (32,164,255), 2, cv2.LINE_AA)
                os.system("mpv audio/triangulo.ogg")
                os.system("mpv audio/dib_trian.ogg")
                os.system("python dibujando/tri.py")
                break
            elif(len(approx) >=4 and len(approx) <=6):
                #n�mero de vertices de un poligono curvo
                vtc = len(approx)
                #obtener los coseno de toda las esquinas
                cos = []
                for j in range(2, vtc+1):
                    cos.append(angle(approx[j%vtc], approx[j-2], approx[j-1]))
                #ordenar cosenos ascendente
                cos.sort()
                #obtener el mas alto y el mas bajo
                mincos = cos[0]
                maxcos = cos[-1]

                #usa los grados obtenidos arriba y el n�mero de vertices para
                #determinar la forma del contorno
                x, y, w, h = cv2.boundingRect(contours[i])
                if(vtc == 4):
                    ar = w / float(h)
                    if ar >= 0.95 and ar <= 1.05:
                        cv2.putText(frame, 'Cuadra', (x, y), cv2.FONT_HERSHEY_SIMPLEX, scale, (109, 45, 215), 2,
                                    cv2.LINE_AA)
                        os.system("mpv audio/cuadrado.ogg")
                        os.system("mpv audio/dib_cuad.ogg")
                        os.system("python dibujando/cuadra.py")
                        break
                    else:
                        cv2.putText(frame, 'Rect', (x, y), cv2.FONT_HERSHEY_SIMPLEX, scale, (109, 45, 215), 2,
                                    cv2.LINE_AA)
                        os.system("mpv audio/rectangulo.ogg")
                        os.system("mpv audio/dib_rect.ogg")
                        os.system("python dibujando/rect.py")
                        break
                elif(vtc == 5):
                    cv2.putText(frame, 'Pent', (x,y), cv2.FONT_HERSHEY_SIMPLEX, scale, (182,236,189), 2, cv2.LINE_AA)
                    os.system("mpv audio/pentagono.ogg")
                    os.system("mpv audio/dib_pent.ogg")
                    os.system("python dibujando/penta.py")
                    break
                elif(vtc == 6):
                    cv2.putText(frame, 'Hex', (x,y), cv2.FONT_HERSHEY_SIMPLEX, scale, (34,20,114), 2, cv2.LINE_AA)
                    os.system("mpv audio/hexagono.ogg")
                    os.system("mpv audio/dib_hexa.ogg")
                    os.system("python dibujando/hexa.py")
                    break
            else:
                #detectar y etiquetar circulo
                area = cv2.contourArea(contours[i])
                x, y, w, h = cv2.boundingRect(contours[i])
                radius = w/2
                if(abs(1 - (float(w)/h)) <= 2 and abs(1 - (area / (math.pi*radius*radius))) <= 0.2):
                    cv2.putText(frame, 'Circulo', (x,y), cv2.FONT_HERSHEY_SIMPLEX, scale, (1,190,229), 2, cv2.LINE_AA)
                    os.system("mpv audio/circulo.ogg")
                    os.system("mpv audio/dib_cir.ogg")
                    os.system("python dibujando/circulo.py")
                    break


        #mostrar los resultados
        out.write(frame)
        cv2.imshow('frame', frame)
        #cv2.imshow('canny', canny)
        if cv2.waitKey(1) == 1048689: #si q es presionada
            break


"""cuando todo haya terminado, se libera la captura"""
cap.release()
cv2.destroyAllWindows()
