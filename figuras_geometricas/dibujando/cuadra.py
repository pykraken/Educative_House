#!/usr/bin/env python
# -*- coding: 850 -*-
# -*- coding: utf-8 -*-
# encoding: latin1


import turtle
import time

turtle.setup(800, 600)
title = turtle.title("dibujando cuadrado")

window = turtle.Screen()
window.bgcolor("white")

flecha = turtle.Turtle()
flecha.color("green")
flecha.pensize(3)
flecha.goto(0, 0)
def fig(lados):
    for i in range(lados):
        flecha.forward(250)
        flecha.left(360/lados)
    flecha.color("blue")
    flecha.goto(0, -25)
    flecha.write("      -------------CUADRADO-------------", False, "left", 50)
    time.sleep(3)
    #window.exitonclick()

fig(4)