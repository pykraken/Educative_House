import turtle
import time

turtle.setup(800, 600, 0, 0)
title = turtle.title("dibujando Círculo")

window = turtle.Screen()
window.bgcolor("white")

flecha = turtle.Turtle()
flecha.color("red")
flecha.pensize(3)
flecha.goto(0, 0)
flecha.speed(100)

def circulo(n, m = 2):
    angulo = 360 // n
    for i in range(n):
        flecha.forward(m)
        flecha.left(angulo)
    flecha.color("blue")
    flecha.goto(0, -25)
    flecha.write(" CÍRCULO", False, "left", 50)
    time.sleep(3)

circulo(360)