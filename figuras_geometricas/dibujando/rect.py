#!/usr/bin/env python
# -*- coding: 850 -*-
# -*- coding: utf-8 -*-
# encoding: latin1


import turtle
import time

turtle.setup(800, 600)
title = turtle.title("dibujando cuadrado")

window = turtle.Screen()
window.bgcolor("white")

flecha = turtle.Turtle()
flecha.pensize(3)
flecha.goto(0, 0)
turtle.forward(150)
turtle.left(90)
turtle.forward(100)
turtle.left(90)
turtle.forward(150)
turtle.left(90)
turtle.forward(100)
turtle.left(90)
flecha.color("blue")
flecha.goto(0, -20)
flecha.write("           Rect ngulo", False, "left", 50)
time.sleep(3)
#window.exitonclick()