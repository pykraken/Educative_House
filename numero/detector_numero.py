#!/usr/bin/env python
# -*- coding: utf-8 -*-
# encoding: latin1

import cv2
import imutils
import  numpy as np
import subprocess
from keras.models import model_from_json
import keras
from keras import backend as k

Unidades = (
    'cero',
    'uno',
    'dos',
    'tres',
    'cuatro',
    'cinco',
    'seis',
    'siete',
    'ocho',
    'nueve'
)

def get_img_contour_thresh(img):
    x, y, w, h = 0, 0, 300, 300
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray, (5, 5), 0)
    ret, thresh1 = cv2.threshold(blur, 175, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    thresh1 = thresh1[y:y + h, x:x + w]
    contours, hierachy = cv2.findContours(thresh1, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2:]
    return img, contours, thresh1

def show_webcam(mirror = False):
    # cargamos el json y creamos el modelo
    json_file = open('modelo.json', 'r')
    loaded_model_json = json_file.read()
    model = model_from_json(loaded_model_json)
    # cargamos los pesos en el modelo
    model.load_weights("modelos.h5")
    print("cargando los modelos del disco")

    model.compile(loss = 'categorical_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])

    cap = cv2.VideoCapture(0)
    while True:
        ret, img = cap.read()
        img, contours, thresh = get_img_contour_thresh(img)
        ans = ''

        if len(contours) > 0:
            contour = max(contours, key=cv2.contourArea)
            if cv2.contourArea(contour) > 500:
                x, y , w, h = cv2.boundingRect(contour)
                newImage = thresh[y:y + h, x:x + w]
                newImage = cv2.resize(newImage, (28, 28))
                newImage = np.array(newImage)
                newImage = newImage.astype('float32')
                newImage /= 255

                if k.image_data_format() == 'channels_first':
                    newImage = newImage.reshape(1, 28, 28)
                else:
                    newImage = newImage.reshape(28, 28, 1)
                newImage = np.expand_dims(newImage, axis=0)
                ans = model.predict(newImage).argmax()

        x, y, w, h = 0, 0, 300, 300
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)
        cv2.putText(img, 'Numero: ' + str(Unidades[ans]), (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (54, 255, 255), 2)

        cv2.imshow("Frame", img)
        cv2.imshow("Contornos", thresh)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    cap.release()
    cv2.destroyAllWindows()

def main():
    show_webcam(mirror=True)

if __name__ == '__main__':
    main()