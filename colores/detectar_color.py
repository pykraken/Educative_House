import cv2
import numpy as np
import os

#capturamos video mediante la webcam
cap = cv2.VideoCapture(0)

while (1):
    _, img = cap.read()
    #convertimos los cuadros RGB a HVS
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    #definiendo el rango del color Rojo
    red_lower = np.array([166, 84, 141], np.uint8)
    red_upper = np.array([186, 255, 255], np.uint8)

    #definiendo el rango del color verde
    green_lower = np.array([66, 122, 129], np.uint8)
    green_upper = np.array([86, 255, 255], np.uint8)

    #definiendo el rango del color azul
    blue_lower = np.array([97, 100, 117], np.uint8)
    blue_upper = np.array([117, 255, 255], np.uint8)

    #definiendo el rango del color amarillo
    yellow_lower = np.array([23, 59, 119], np.uint8)
    yellow_upper = np.array([54, 255, 255], np.uint8)

    #definiendo el rango del color naranja
    orange_lower = np.array([0, 50, 80], np.uint8)
    orange_upper = np.array([20, 255, 255], np.uint8)

    #buscando el rango del color en la imagen
    red = cv2.inRange(hsv, red_lower, red_upper)
    green = cv2.inRange(hsv, green_lower, green_upper)
    blue = cv2.inRange(hsv, blue_lower, blue_upper)
    yellow = cv2.inRange(hsv, yellow_lower, yellow_upper)
    orange = cv2.inRange(hsv, orange_lower, orange_upper)

    #transformación morfologica, dilatación
    kernal = np.ones((5,5), "uint8")

    red = cv2.dilate(red, kernal)
    res = cv2.bitwise_and(img, img, mask = red)

    green = cv2.dilate(green, kernal)
    res1 = cv2.bitwise_and(img, img, mask = green)

    blue = cv2.dilate(blue, kernal)
    res2 = cv2.bitwise_and(img, img, mask = blue)

    yellow = cv2.dilate(yellow, kernal)
    res3 = cv2.bitwise_and(img, img, mask = yellow)

    orange = cv2.dilate(orange, kernal)
    res4 = cv2.bitwise_and(img, img, mask = orange)

    #rastreando el color rojo
    (_, contours, hierarchy) = cv2.findContours(red, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if (area > 300):
            x, y , w, h = cv2.boundingRect(contour)
            img = cv2.rectangle(img, (x, y), (x + w, y + h), (86, 255, 255), 2)
            cv2.putText(img, "color ROJO", (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (54, 255, 255))
        break

    #rastreando el color verde
    (_, contours, hierarchy) = cv2.findContours(green, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if(area > 300):
            x, y, w, h = cv2.boundingRect(contour)
            img = cv2.rectangle(img, (x, y), (x +w, y + h), (166, 84, 141), 2)
            cv2.putText(img, "color VERDE", (x, y), cv2.FONT_HERSHEY_SIMPLEX, 2.0, (166, 84, 141))
        break

    #rastreando el color azul
    (_, contours, hierarchy) = cv2.findContours(blue, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if (area > 300):
            x, y, w, h = cv2.boundingRect(contour)
            img = cv2.rectangle(img, (x, y), (x + w, y + h), (66, 122, 129), 2)
            cv2.putText(img, "color AZUL", (x, y), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0, 50, 80))
        break

    #rastreando el color amarillo
    (_, contours, hierarchy) = cv2.findContours(yellow, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if(area > 300):
            x, y, w, h = cv2.boundingRect(contour)
            img = cv2.rectangle(img, (x, y), (x + w, y + h), (166, 84, 141), 2)
            cv2.putText(img, "color AMARILLO", (x, y), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (97, 100, 117))
        break

    #rastreando el color movimiento naranja
    (_, contours, hierarchy) = cv2.findContours(orange, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    for pic, contour in enumerate(contours):
        area = cv2.contourArea(contour)
        if(area > 300):
            x, y, w, h = cv2.boundingRect(contour)
            img = cv2.rectangle(img, (x, y), (x +w, y + h), (66, 122, 129), 2)
            cv2.putText(img, "color NARANJA", (x, y), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (166, 84, 141))
        break


    cv2.imshow("Color Rastreado", img)
    if cv2.waitKey(10) & 0xFF == ord('q'):
        cap.release()
        cv2.destroyAllWindows()
        break