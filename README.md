|# comenzamos 3 septiembre 2018 con un proyecto de casa educativa veamos a donde llega

- ## Educative House
  

  un proyecto que comenzó como idea en mi tesis de grado,  lo que se intenta realizar es aprendizaje orientado a niños de preescolar para que aprendan a interpretar las vocales, número, colores y figuras geométricas.

  se utilizan muchos metodos y todo esta desarrollado en python bajo licencia GPLv3. Las librerias de python utilizadas para este trabajo son:
    - Python 3.X
    - Opencv
    - math librerias
    - tensorflow
  
  acontinuación guardo mi configuracion de vim para interesados solo basta con cambiar vimrc a .vimrc y cuando entren a vim :PlugInstall and :PlugUpdate
